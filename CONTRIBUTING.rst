============
Contributing
============

We are welcoming all kinds of contributions:

* bug reports
* proposals for changes and new features
* documentation
* fixes for the language in GUI and web (Main developers don't speak English natively)
* bug fixes
* tests
* refactorings
* ...