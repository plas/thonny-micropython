import websocket
import threading
from time import sleep

def on_message(ws, message):
    print(message, end="")

def on_close(ws):
    print("### closed ###")

websocket.enableTrace(True)
ws = websocket.WebSocketApp("ws://192.168.1.226:8266", on_message = on_message, on_close = on_close)
wst = threading.Thread(target=ws.run_forever)
wst.daemon = True
wst.start()

conn_timeout = 5
while not ws.sock.connected and conn_timeout:
    sleep(1)
    conn_timeout -= 1
    
while ws.sock.connected:
    inp = (input()
           .replace("\\n", "\r\n")      # end of command in normal mode
           .replace("\\x01", "\x01")    # switch to raw mode
           .replace("\\x02", "\x02")    # switch to normal mode
           .replace("\\x03", "\x03")    # interrupt
           .replace("\\x04", "\x04"))   # end of command in raw mode

    if inp == "exit":
        ws.close()
    else:
        ws.send(inp)
