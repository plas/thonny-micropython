This repository is obsolete.

Since version 3.0.0b1, Thonny has basic MicroPython support built in.

See https://github.com/thonny/thonny/wiki/MicroPython for more info.