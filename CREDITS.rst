=======
Credits
=======

Origin
------
thonny-micropython grew out from version 0.1 of `thonny-microbit <https://bitbucket.org/KauriRaba/thonny-microbit>`_.

Libraries and resources
-----------------------
* pyserial (https://pythonhosted.org/pyserial/) 

Source contributors and frequent bug-reporters
----------------------------------------------
* Aivar Annamaa
* Kauri Raba

Please let us know if we have forgotten to add your name to this list! Also, let us know if you want to remove your name.